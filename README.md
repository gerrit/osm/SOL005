<!--
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
implied.
See the License for the specific language governing permissions and
limitations under the License
-->
# SOL005

OSM client library and console script

## Build docker image to test OpenAPI spec

```bash
docker build -t osm/sol005-devel .
```

## Test OpenAPI spec with swagger-cli and speccy

```bash
docker run -it -v .:/root/SOL005 -w /root/SOL005 osm/sol005-devel /root/SOL005/devops-stages/stage-test.sh
```

